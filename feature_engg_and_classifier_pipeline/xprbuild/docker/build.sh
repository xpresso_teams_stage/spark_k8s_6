#! /bin/bash
## This script is used to build the docker image for the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user

DOCKER_IMAGE_NAME=${1}
TAG=${2}
current_folder=${ROOT_FOLDER}/xprbuild/docker

echo "Now logging into docker registry"
docker login ${DOCKER_REGISTRY} -u admin -p Abz00ba@123

cmd="docker build -t ${DOCKER_IMAGE_NAME}:${TAG} -f $current_folder/Dockerfile ${ROOT_FOLDER}"
echo "Building the docker image"
echo "Docker build command -> $cmd"
exec $cmd
