import threading
import time
import os
from datetime import datetime
from pyspark.ml.pipeline import Pipeline
from pyspark.ml.pipeline import PipelineModel
from pyspark.ml.base import Estimator, Transformer
from one_hot_encoder.app.mongo import MongoPersistenceManager

from .controller_stub import ControllerStub

class AbstractPipelineComponent:
    def __init__(self, run_id, is_thread_needed=True):
        print("Initializing component")
        # note run ID
        self.run_id = run_id
        # set run_status
        self.run_status = "IDLE"
        # state of componet (to be saved on pause, and loaded on restart)
        self.state = None
        # status of component (to be reported on periodic basis - consists of status dict and metrics dict)
        self.status = None
        # output of component (to be stored on disk on completion)
        self.output = None
        # final results of component (to be stored in database on completion)
        self.results = None

        self.run_status_thread = None
        self.is_thread_needed = is_thread_needed
        self.controller = ControllerStub()
        print("Component initlaized")

    def start(self, *args):
        print("Parent component starting", flush=True)
        self.run_id = args[0]
        # check status immediately and take action if required
        self.get_run_status()

        # start thread to check run status
        self.run_status_thread = threading.Thread(target=self.check_run_status)
        self.is_thread_needed = True
        self.run_status_thread.start()
        self.controller.pipeline_component_started(self.run_id, self.name)

    def terminate(self):
        print("Parent component terminating", flush=True)
        self.controller.pipeline_component_terminated(self.run_id, self.name)
        self.is_thread_needed = False
        os._exit(0)

    def terminate_without_exit(self):
        print("Parent component terminating without exit", flush=True)
        self.controller.pipeline_component_terminated(self.run_id, self.name)
        self.is_thread_needed = False

    def pause(self):
        print("Parent component saving state and exiting", flush=True)
        self.controller.pipeline_component_paused(self.run_id, self.name,
                                                  self.state)
        self.is_thread_needed = False
        os._exit(0)

    def restart(self):
        print("Parent component restarting", flush=True)
        state = self.controller.pipeline_component_restarted(self.run_id,
                                                             self.name)
        if state is not None:
            # self.state = state
            self.controller.update_pipeline_run_status(self.run_id, "RUNNING")

        else:
            print(
                "This component already completed execution. Some other component needs to restart. Terminating",
                flush=True)
            self.terminate_without_exit()

    def completed(self):
        print(f"Parent component completed {self.name}", flush=True)
        self.controller.pipeline_component_completed(self.run_id, self.name)
        self.is_thread_needed = False

    def report_pipeline_status(self, status):
        print("Parent component reporting status", flush=True)
        self.controller.report_pipeline_status(self.run_id, self.name, status)

    def check_run_status(self):
        while self.is_thread_needed:
            self.get_run_status()
            time.sleep(5)
        print(f"Stopping check run status thread for {self.name}")

    def get_run_status(self):
        self.run_status = self.controller.get_pipeline_run_status(self.run_id)
        print(
            "Run ID: {}, Time:{}, Component: {}, Status: {}".format(
                str(self.run_id),
                datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                self.name,
                self.run_status))
        print("RUN STATUS: {}".format(self.run_status), flush=True)
        if self.run_status == "IDLE":
            pass
        elif self.run_status == "RUNNING":
            pass
        elif self.run_status == "TERMINATED":
            print("Terminating component", flush=True)
            self.terminate()
        elif self.run_status == "PAUSED":
            print("Pausing component", flush=True)
            self.pause()
        elif self.run_status == "RESTARTED":
            print("Restarting component", flush=True)
            self.restart()
        else:
            print("No action required...continuing", flush=True)


class AbstractSparkComponent(AbstractPipelineComponent):
    def __init__(self, run_id, is_thread_needed=True):
        super().__init__(run_id, is_thread_needed=is_thread_needed)


class AbstractSparkPipelineEstimator(AbstractSparkComponent):

    def __init__(self, run_id, is_thread_needed=True):
        super().__init__(run_id, is_thread_needed=True)


class AbstractSparkPipelineTransformer(AbstractSparkComponent):
    def __init__(self, run_id, is_thread_needed=True):
        super().__init__(run_id, is_thread_needed=True)


class XprPipeline(Pipeline, AbstractSparkComponent):

    def __init__(self, name, spark, run_id, stages=None):
        self.name = name
        self.spark = spark
        Pipeline.__init__(self, stages=stages)
        AbstractSparkComponent.__init__(self, run_id, is_thread_needed=False)

    def _fit(self, dataset):
        stages = self.getStages()
        for stage in stages:
            run_id = stage.run_id
            if not (isinstance(stage, Estimator) or isinstance(stage, Transformer)):
                raise TypeError(
                    "Cannot recognize a pipeline stage of type %s." % type(stage))

        # Xpr start
        component_names = [component.name for component in stages]
        print(f'In XprPipeline _fit', flush=True)
        print(f'All components = {component_names}', flush=True)
        pm = MongoPersistenceManager()
        runs = pm.find('runs', {'run_name' : run_id})
        paused_at=None
        if runs:
            run = runs[0]
            paused_at = run.get('paused_at', None)

        index_of_stage_paused_at = None
        for index, stage in enumerate(stages):
            if stage.name == paused_at:
                index_of_stage_paused_at = index

        resume_index = 0
        if index_of_stage_paused_at:
            resume_index = index_of_stage_paused_at+1
 
        skip_stages = stages[:resume_index]
        skip_stage_names = [stage.name for stage in skip_stages]
        print(f'index_of_stage_paused_at= {index_of_stage_paused_at}', flush=True)
        print(f'resume_index= {resume_index}', flush=True)
        print(f'skip_stage_names= {skip_stage_names}', flush=True)

        # TODO - how to skip stage in loop below for restarting.
        # TODO tricky transformers.append for estimators
        # TODO - Reading state at the beginning of the job/pipeline
        # Xpr end

        indexOfLastEstimator = -1
        for i, stage in enumerate(stages):
            if isinstance(stage, Estimator):
                indexOfLastEstimator = i
        transformers = []
        p_start_time = time.time()
        print(f'indexOfLastEstimator={indexOfLastEstimator}')
        for i, stage in enumerate(stages):
            start_time = time.time()
            print(f'Now running stage={i} and name={stage.name} with run_id={self.run_id}', flush=True)
            if i <= indexOfLastEstimator:
                if isinstance(stage, Transformer):
                    transformers.append(stage)
                    dataset = stage.transform(dataset)
                else:  # must be an Estimator
                    model = stage.fit(dataset)
                    transformers.append(model)
                    if i < indexOfLastEstimator:
                        dataset = model.transform(dataset)
            else:
                transformers.append(stage)

            print(f'Now DONE running stage={i} and name={stage.name} with run_id={self.run_id}', flush=True)
            # some component completed, check if paused, save the state, free df resources and exit
            # we dont want thread to save the state in background for spark-jobs
            # to avoid concurrent writes and incosistenices
            # should be done at the end of a component's execution
            n = 1
            print(f'transformers length={len(transformers)}', flush=True)
            print(f"Time taken for component {stage.name} is {time.time() - start_time}" , flush=True)
            print(f'Sleeping @ component {stage.name} for {n}sec', flush=True)
            time.sleep(n)
            print(f'Resuming from sleep for {stage.name}', flush=True)
            self.state = dataset
            stage.state = dataset
            status = {'status' : f'Component completed {stage.name}', 'metrics' : {}}
            stage.report_pipeline_status(status)
            stage.get_run_status()
            print(f'Completed after Resuming for {stage.name}', flush=True)
        self.completed()
        print(f"Time taken for full pipeline is {time.time() - p_start_time}" , flush=True)

        print(f'Done now returning PipelineModel', flush=True)
        return PipelineModel(transformers)