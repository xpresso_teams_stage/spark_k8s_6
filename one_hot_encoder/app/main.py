
from pyspark.sql import SparkSession


if __name__ == "__main__":

    input_path = "data/sample_data.txt"

    spark = SparkSession \
        .builder \
        .appName("DataFrameExample") \
        .getOrCreate()

    spark.stop()