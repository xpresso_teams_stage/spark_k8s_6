
from pyspark.sql import SparkSession

from .assembler import CustomVectorAssembler


if __name__ == "__main__":

    input_path = "data/sample_data.txt"

    spark = SparkSession \
        .builder \
        .appName("DataFrameExample") \
        .getOrCreate()
    
    # if previous stage was pause, how to I get the dataframe for that stage?
    # if previous stage was pause, how to I get the dataframe for that stage?
    assembler = CustomVectorAssembler()
    df = None
    assembler.transform(df)

    spark.stop()